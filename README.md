# SWH R Code Library Related Functions

I am keeping some of my useful library related R functions in here so I can find them.

Here's the list so far:

- Title cleanup function
- New columns for Library of Congress class and subclass (draft)
- NC triangle area schools color theme 
- Split column at pipe and cleanup columns - functions




